Install

cd  
git clone https://github.com/dji-sdk/Onboard-SDK  
cd Onboard-SDK  
git checkout 3.7  
mkdir build  
cd build  
cmake ..  
make djiosdk-core  
sudo make install  
cd  
mkdir mbz_ws  
cd mbz_ws  
git clone http://gitlab.com/ngreig85/mbzirc.git  
mv mbzirc src  
catkin build  

DJI SDK Reference
http://wiki.ros.org/dji_sdk

192.168.50.2 phot-Blade
192.168.50.4 phot-NUC7i7BNKQ
192.168.50.5 quad2-PC
