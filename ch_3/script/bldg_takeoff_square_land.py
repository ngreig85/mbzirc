#!/usr/bin/env python

import rospy
import math

from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import PointStamped
from sensor_msgs.msg import Joy

from dji_sdk.srv import *


class Drone:
	def local_pos_callback(self, data):
	
		self.x_pos = data.point.x
		self.y_pos = data.point.y
		self.z_pos = data.point.z
	
	def init(self):
		rospy.init_node("square")
		rospy.Subscriber("/dji_sdk/local_position", PointStamped, self.local_pos_callback)
		#self.pub = rospy.Publisher("/dji_sdk/flight_control_setpoint_generic", Joy, queue_size=10)
		self.pub = rospy.Publisher("/dji_sdk/flight_control_setpoint_generic", Joy, queue_size=10)
		self.x_pos = 0.0
		self.y_pos = 0.0
		self.z_pos = 0.0
		self.yaw = 0.0
		self.state = 0
		self.first = 1
		self.flag = 0x40 | 0x00 | 0x08 | 0x02 | 0x00
		print "gain control"
		rospy.wait_for_service('dji_sdk/sdk_control_authority')
		try:
			api = rospy.ServiceProxy('dji_sdk/sdk_control_authority', SDKControlAuthority)
			resp = api(1)
			print resp
		except rospy.ServiceException, e:
			print "Service failed: %s"%e
		print "set local position ref"		
		rospy.wait_for_service('dji_sdk/set_local_pos_ref')		
		try:
			pos = rospy.ServiceProxy('dji_sdk/set_local_pos_ref', SetLocalPosRef)
			resp = pos()
			print resp
		except rospy.ServiceException, e:
			print "Service failed: %s"%e
		print "end init"

	def control(self):
		msg = Joy()
		msg.buttons = []
		if self.state == 0:
			if self.first:
				print "takeoff"
				rospy.wait_for_service('dji_sdk/drone_task_control')
				try:
					takeoff = rospy.ServiceProxy('dji_sdk/drone_task_control', DroneTaskControl)
					resp = takeoff(4)
					print resp
				except rospy.ServiceException, e:
					print "Service call failed: %s"%e
				self.first = 0
			#print self.z_pos			
			if self.z_pos > 1.15:
				print "takeoff success"
				self.state = 1
				self.first = 1
		elif self.state == 1:
			if self.first:
				print "side one"
				self.first = 0
				self.left = 0
			#print self.x_pos
			if math.fabs(self.x_pos) < .1 and math.fabs(self.y_pos) < .1 and self.left: 
				msg.axes = [0, 0, 0, 0, self.flag]
				self.pub.publish(msg)
				self.state = 2
				self.first = 1
			else:
				#x_vel = (2 - self.x_pos)
				y_vel = .25
				yaw_vel = -0.0556
				if self.z_pos < 1.15:
					z_vel = .25
				else:
					z_vel = 0
				msg.axes = [0, y_vel, z_vel, yaw_vel, self.flag]
				self.pub.publish(msg)
				self.left = 1
			
		else:
			if self.first:
				print "land"
				rospy.wait_for_service('dji_sdk/drone_task_control')
				try:
					land = rospy.ServiceProxy('dji_sdk/drone_task_control', DroneTaskControl)
					resp = land(6)
					print resp
				except rospy.ServiceException, e:
					print "Service call failed: %s"%e
				self.first = 0
			if self.z_pos < .1:
				print "success"

	
if __name__ == '__main__':
	drone = Drone()
	drone.init()
	while(not rospy.is_shutdown()):
		drone.control()
	rospy.spin()
	
