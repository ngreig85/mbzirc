#!/usr/bin/env python

import rospy
import math
import numpy as np
import tf
import sensor_msgs.point_cloud2 as pc2
import laser_geometry.laser_geometry as lg


from sensor_msgs.msg import LaserScan, PointCloud2

class Lidar:

	def init(self):

		#initialize node
		rospy.init_node('lider')

		#subscribe to laser scan topic (raw hokuyo output)
		#rospy.Subscriber("/scan", LaserScan, self.ScanCallBack)
		rospy.Subscriber("/converted_pc", PointCloud2, self.pc_callback)

		self.lp = lg.LaserProjection()

		self.in_callback = False
		self.calculating = False
		self.ranges = []
		self.min_range = 0.0
		self.angle_start = 0.0
		self.angle_step = 0.0
		self.angle_min = 0.0

		self.prev_min = 0.0
		self.pc2_msg = PointCloud2()

	def pc_callback(self, data):
		
		if not self.calculating:
			self.in_callback = True		
			self.points = []
			self.points_polar = []

			for p in pc2.read_points(data, field_names = ("x","y"), skip_nans = False):
				self.points_polar.append((math.sqrt(p[0] ** 2 + p[1] ** 2), math.atan2(p[1], p[0])))			
				self.points.append(p)
			self.in_callback = False
		


	def ScanCallBack(self, data):

		#raw ranges from laser scan
		#self.ranges = data.ranges	#meters*
		
	
		#minimum range as reported by the LIDAR, ranges below this should be ignored
		#constant
		#self.min_range = data.range_min	#meters
	
		#angle increment in between measurements
		#constant
		#self.angle_step = data.angle_increment	#radians
	
		#starting angle of laser scan
		#constant
		#self.angle_start = data.angle_min	#radians
		
		self.pc2_msg = self.lp.projectLaser(data)
		

	#function to calculate the ditance of the drone from the wall
	#this function assumes the robot will pass in front of windows, corners, etc and thus uses the raw laser scan data
	def get_wall_distance(self):
		
		#self.calculating = True
		#self.clean_points = []

		#i = 1
		#while i < len(self.points_polar) - 1:
		#	prev = self.points_polar[i - 1][0]
		#	cur = self.points_polar[i][0]
		#	next = self.points_polar[i + 1][0]
		#	if not (math.fabs(prev - cur) > 0.1 and math.fabs(cur - next) > 0.1):
		#		self.clean_points.append(self.points_polar[i])
		#	i += 1
		#self.calculating = False
		min_val = (15, 0)
		for n in self.points_polar:
			dist = n[0]
			if dist < min_val[0] and dist > .1:
				min_val = n
		
		d = math.fabs(min_val[0] * math.cos(min_val[1]))

		#i = 1
		#while i < len(self.dirty_ranges) - 1:
		#	prev = self.dirty_ranges[i-1] * math.fabs(math.cos((i-1) * self.angle_step))
		#	cur = self.dirty_ranges[i] * math.fabs(math.cos(i * self.angle_step))
		#	next = self.dirty_ranges[i+1] * math.fabs(math.cos((i+1) * self.angle_step))
		#	if not(math.fabs(prev - cur) > 0.1 and math.fabs(cur - next) > 0.1):
		#		self.ranges.append(self.dirty_ranges[i])
		#	i+=1
		#intiialize min_val to possible final value
		#check tihs line if ranges seem wrong
		#min_val = 15
		#i=0
		#index of min value		
		#i_min = 0
		#for n in self.ranges:
			#clean range data
		#	if n >= self.min_range:
		#		if n < min_val:
		#			min_val = n
		#			i_min = i
		#	i+=1
		#if math.fabs(min_val - self.prev_min) > .1:
		#	min_val = self.prev_min
		#else:
		#	self.prev_min = min_val
		#calculate angle of minimum value
		#self.angle_min = (i_min * self.angle_step)
		#calculate distance from wall
		#d = min_val * math.fabs(math.cos(self.angle_start + self.angle_min - math.pi/2))
		#print min_val, (self.angle_min + self.angle_start - math.pi/2)*360/2/math.pi
		return d

if __name__ == '__main__':

	lider = Lidar()
	lider.init()
	rospy.wait_for_message("converted_pc", PointCloud2)
	#lider.prev_min = lider.get_wall_distance()
	#lider.prev_min = 1.1
	while not rospy.is_shutdown():
		
		print lider.get_wall_distance()
	rospy.spin()

