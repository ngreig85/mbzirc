#!/usr/bin/env python

import rospy

from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import Twist, Vector3

from dji_sdk.srv import *

rospy.init_node("takeoff_land")
rospy.wait_for_service('dji_sdk/sdk_control_authority')
try:
	control = rospy.ServiceProxy('dji_sdk/sdk_control_authority', SDKControlAuthority)
	resp = control(1)
	print resp
except rospy.ServiceException, e:
	print "Service failed: %s"%e
print "end init"
print "takeoff"
rospy.wait_for_service('dji_sdk/drone_task_control')
try:
	takeoff = rospy.ServiceProxy('dji_sdk/drone_task_control', DroneTaskControl)
	resp = takeoff(4)
	print resp
except rospy.ServiceException, e:
	print "Service call failed: %s"%e
print "takeoff success"
rospy.sleep(5)
rospy.wait_for_service('dji_sdk/drone_task_control')
print "land"
try:
	land = rospy.ServiceProxy('dji_sdk/drone_task_control', DroneTaskControl)
	resp = land(6)
except rospy.ServiceException, e:
	print "Service call failed: %s"%e
rospy.loginfo("success")
