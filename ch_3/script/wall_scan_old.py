#!/usr/bin/env python

import rospy
import math
import numpy as np
import tf
import sensor_msgs.point_cloud2 as pc2

from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import PointStamped
from sensor_msgs.msg import LaserScan, PointCloud2, Joy

from dji_sdk.srv import *

#height of first floor
HEIGHT_INITIAL = 1.15	#meters

#height of second floor
HEIGHT_SECONDARY = 15.0	#meters

#tolerance when deciding if we reahced a goal
POINT_TOLERANCE = 0.1	#meters

#global robot velocities
X_VELOCITY = 0.25
Y_VELOCITY = 0.25
Z_VELOCITY = 0.25
ROLL_VELOCITY = 1.0
PITCH_VELOCITY = 1.0
YAW_VELOCITY = 0.25
FORWARD_VELOCITY = 0.25

#follow distance from building
BUILDING_DIST = 3.0	#meters

class wall:


	def init(self):
		
		#initialize node
		rospy.init_node('wall_scan')

		#subscribe to laser scan topic (raw hokuyo output)
		rospy.Subscriber("/shadow_filtered", LaserScan, self.ScanCallBack)

		#subscribe to laser scan converter topic (converts raw laser scan data to point cloud)
		#rospy.Subscriber("/converted_pc", PointCloud2, self.PointCloudCallBack)

		#subscribe to odometry data
		#rospy.Subscriber("/dji_sdk/local_position", PointStamped, self.local_pos_callback)

		#publisher for robt velocity
		self.pub = rospy.Publisher("/dji_sdk/flight_control_setpoint_generic", Joy, queue_size=10)

		#tf listener
		#self.listener = tf.TransformListener()

		rospy.loginfo("end init")

		self.first_it = True

		self.ranges = []
		self.points = []
		self.min_range = 0.0
		self.angle_start = 0.0
		self.angle_step = 0.0
		self.angle_min = 0.0
		self.state = 0

		self.goal_height = 5.0
		self.wantedrange = 5
		self.Tps = 0
		self.changeX = 0
		self.changeY = 0
		self.changeX2 = 0
		self.changeY2 = 0
		self.step = 0
		self.startposeX = 0
		self.startposeY = 0
		self.x_pos = 0
		self.y_pos = 0
		self.z_pos = 1.15
		self.pose = [0, 0, 0]
		self.flag = 0x40 | 0x00 | 0x08 | 0x02 | 0x00

		print "gain control"
		rospy.wait_for_service('dji_sdk/sdk_control_authority')
		try:
			api = rospy.ServiceProxy('dji_sdk/sdk_control_authority', SDKControlAuthority)
			resp = api(1)
			print resp
		except rospy.ServiceException, e:
			print "Service failed: %s"%e
		#print "set local position ref"		
		#rospy.wait_for_service('dji_sdk/set_local_pos_ref')		
		#try:
		#	pos = rospy.ServiceProxy('dji_sdk/set_local_pos_ref', SetLocalPosRef)
		#	resp = pos()
		#	print resp
		#except rospy.ServiceException, e:
		#	print "Service failed: %s"%e
		print "end init"

	#callback for laser scan subscriber
	#grabs ranges and laser scan configuration data
	#may need to add time between scans when drone is moving fast
	def ScanCallBack(self, data):

		#raw ranges from laser scan
		self.ranges = data.ranges	#meters
		
		#minimum range as reported by the LIDAR, ranges below this should be ignored
		#constant
		self.min_range = data.range_min	#meters
	
		#angle increment in between measurements
		#constant
		self.angle_step = data.angle_increment	#radians
	
		#starting angle of laser scan
		#constant
		self.angle_start = data.angle_min	#radians
		
	#callback for point cloud converter subscriber
	#transforms point cloud from laser scan to list of points in world frame
	#def PointCloudCallBack(self, data):
		
		#get individual transforms between frames
		#we want to go from laser0_frame (frame for laser scan ranges) to world
		#must be done individually, may be a way to combine
		#W1 = self.listener.lookupTransform('/laser0_frame', '/base_link', rospy.Time(0))
		#W2 = self.listener.lookupTransform('/base_link', '/base_stabilized', rospy.Time(0))
		#W3 = self.listener.lookupTransform('/base_stabilized', '/base_footprint', rospy.Time(0))
		#W4 = self.listener.lookupTransform('/base_footprint', '/world', rospy.Time(0))

		#combine linear transforms to one transform
		#we don't need to worry about rotation transforms, as these are just points
		#trans = W1[0] + W2[0] + W3[0] + W4[0]

		#read points from point cloud
		#for p in pc2.read_points(data, field_names = ("x","y","z"), skip_nans = False):
			
			#apply transform to each point
		#	p2 = [p[0]+trans[0],p[1]+trans[1],p[2]+trans[2]]			
		#	self.points.append(p2)

	#get current robot postiion
	def local_pos_callback(self, data):
	
		self.x_pos = data.point.x
		self.y_pos = data.point.y
		self.z_pos = data.point.z
		self.pose = [self.x_pos, self.y_pos, self.z_pos]
		#print "callback"

	#function to navigate to a specified pose in the world frame
	#returns Vector3 for linear and angular velocities
	#should be called periodically for up-to-date velocities


	#fucntion to navigate drone to specific height in meters	
	#identical to nav_to_pose, but uses current location for all inputs except z
	def nav_to_z(self, dist):
		d = dist - self.pose[2]
		return d * Z_VELOCITY


	def nav_to_range(self, dist):
		d = dist - self.get_wall_distance()
		return d * X_VELOCITY

	#based on two points, get the angle in the XY plane
	#radians
	def get_angle(self, pt1, pt2):
		dx = pt1[0] - pt2[0]
		dy = pt1[1] = pt2[1]
		return math.atan2(dy, dx)

	#function to calculate the ditance of the drone from the wall
	#this function assumes the robot will pass in front of windows, corners, etc and thus uses the raw laser scan data
	def get_wall_distance(self):
		
		#intiialize min_val to possible final value
		#check tihs line if ranges seem wrong
		min_val = 15.0
		i=0
		#index of min value		
		i_min = 0
		for n in self.ranges:
			#clean range data
			if n >= self.min_range:
				if n < min_val:
					min_val = n
					i_min = i
			i+=1
		#calculate angle of minimum value
		self.angle_min = (i_min * self.angle_step)
		#calculate distance from wall
		d = min_val * math.fabs(math.cos(self.angle_start + self.angle_min - math.pi/2))
		print d
		return d		
	
	def vel_cutoff(self, vel, max_vel):
		if math.fabs(vel) > max_vel:
			return max_vel
		else:
			return vel

	#checks if two points are "close enough" to each other
	def close_to_point(self, p1, p2):
		if (math.fabs(p1[0] - p2[0]) < POINT_TOLERANCE) and (math.fabs(p1[1] - p2[1]) < POINT_TOLERANCE) and (math.fabs(p1[2] - p2[2]) < POINT_TOLERANCE):
			return True
		else:
			return False
	
	#identical to close_to_point but ignores z coordinate
	def close_to_point_XY(self, p1, p2):
		if (math.fabs(p1[0] - p2[0]) < POINT_TOLERANCE) and (math.fabs(p1[1] - p2[1]) < POINT_TOLERANCE):
			return True
		else:
			return False
	#identical to close_to_point but ingores X and Y coordinates
	def close_to_point_Z(self, p1, p2):
		if (math.fabs(p1[2] - p2[2]) < POINT_TOLERANCE):
			return True
		else:
			return False		

	#main control loop
	def Control(self):

		#prepare a twist message
		msg = Joy()
		x_vel = 0
		y_vel = 0
		z_vel = 0
		yaw_vel = 0

		#takeoff
		if self.state == 0:
			#log starting position on first it
			if self.first_it:
				self.first_it = False
				self.goal_height = HEIGHT_INITIAL
				self.start_pose = self.pose
				self.goal_pose = [self.pose[0], self.pose[1], self.goal_height]
				print "takeoff"
				rospy.wait_for_service('dji_sdk/drone_task_control')
				try:
					takeoff = rospy.ServiceProxy('dji_sdk/drone_task_control', DroneTaskControl)
					resp = takeoff(4)
					print resp
				except rospy.ServiceException, e:
					print "Service call failed: %s"%e
			#print self.z_pos
			#z_vel = self.nav_to_z(self.z_pos)
			#advance to next state when at desired height
			#if self.close_to_point_Z(self.pose, self.goal_pose):
			if 1 == 1:
				self.state = 1
				self.first_it = True
				print "end takeoff"

		#building approach
		elif self.state == 1:
			if self.first_it:
				self.first_it = False
				print "approach"
			#z_vel = self.nav_to_z(self.goal_height)
			x_vel = self.nav_to_range(BUILDING_DIST)
			if math.fabs(self.get_wall_distance() - BUILDING_DIST) < POINT_TOLERANCE:
				self.bldg_pose = self.pose
				self.at_home = True
				self.state = 2
				self.first_it = True
				print "end approach"

		#building scan
		elif self.state == 2:
			if self.first_it:
				self.first_it = False
				print "scan"
			#z_vel = self.nav_to_z(self.goal_height)
			x_vel = self.nav_to_range(BUILDING_DIST)
			y_vel = FORWARD_VELOCITY
			#work on this			
			yaw_vel = (self.get_wall_distance() - BUILDING_DIST) * YAW_VELOCITY
			#yaw_vel = self.nav_to_pos(0, 0, 0, 0, 0, self.angle_min - math.pi/2)[1].z
			
			#if self.close_to_point_XY(self.pose, self.bldg_pose): 
			#	if not self.at_home:
			#		self.state = 5
			#elif self.at_home:
			#	self.at_home = False

		else:
			print "land"
			rospy.wait_for_service('dji_sdk/drone_task_control')
			try:
				land = rospy.ServiceProxy('dji_sdk/drone_task_control', DroneTaskControl)
				resp = land(6)
				print resp
			except rospy.ServiceException, e:
				print "Service call failed: %s"%e
		x_vel = self.vel_cutoff(x_vel, X_VELOCITY)
		y_vel = self.vel_cutoff(y_vel, Y_VELOCITY)
		z_vel = self.vel_cutoff(z_vel, Z_VELOCITY)
		msg.axes = [x_vel, y_vel, z_vel, yaw_vel, self.flag]
		self.pub.publish(msg)
		#print(self.yaw)
			

if __name__ == '__main__':

	WallE = wall()
	WallE.init()
	rospy.wait_for_message("scan", LaserScan)
	
	while not rospy.is_shutdown():
		
		WallE.Control()
	rospy.spin()


